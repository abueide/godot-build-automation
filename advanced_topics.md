# Advanced Topics

## Using Godot 3.1 (or other version)

I will try to continue to support old versions of Godot when I can, however the
`registry.gitlab.com/greenfox/godot-build-automation:latest` container will
usually be the latest version of Godot and the latest version of the
`./build.sh` script. I will not support old version of the build script.

The naming convention of containers will be
`:${GODOT_VERSION}_${SCRIPT_VERSION}`.

If you would like to use a different version of Godot or build script, [go to
the contianer regestry of this
project](https://gitlab.com/greenfox/godot-build-automation/container_registry)
and find the version that most suits your needs. At the time of writing these
versions exist:

|Container | Godot Version | Script Version|
|-|-|-|
|registry.gitlab.com/greenfox/godot-build-automation:latest | 3.2.1 | 1.3|
|registry.gitlab.com/greenfox/godot-build-automation:3.2.1 | 3.2.1 | 1.3|
|registry.gitlab.com/greenfox/godot-build-automation:3.2_1.3 | 3.2 | 1.3|
|registry.gitlab.com/greenfox/godot-build-automation:3.1.1_1.3 | 3.1.1 | 1.3|
|registry.gitlab.com/greenfox/godot-build-automation:3.2_mono_1.4 | 3.2 w/ Mono (C#) | 1.4 (this is expriemental)|

Replace the first line of the `.gitlab-ci.yml` file with `image:
${CONTAINER_STRING}` for the version you would like to use. Click the little
clipboard image from the container regestry page to copy the string you need
then paste it into your `./gitlab-ci.yml` file.

## Compiling Godot Engine Itself

If you need Modules in Godot, then you will need to compile Godot itself. This
is not yet supported, but I will at some point add a container that is intended
for building Godot itself. I have some incomplete helper scripts that I used for
a project that needed Godot Modules in the `./GodotCompiler` directory. This
section remains a work in progress. It will get more polish the next time I need
to build Godot with a module.

## Using A Custom Build of Godot

You might have a custom build of Godot (for example with C++ Modules) that you
would like to use. You could build your own docker container with your version
of Godot installed. I don't do this, instead I use the same docker container
here and overwrite the files I need.

First, I'm assuming you know how to compile Godot for all your target platforms.
I won't cover that here. [Godot has pretty good documentation on the
subject.](https://docs.godotengine.org/en/latest/development/compiling/index.html)

There is one "gotcha" here. You will need to compile "Godot Headless", which is
the Godot version that can run on a server without a screen and build a Godot
project. This is a bit confusing and poorly documented. To get "Godot Headless"
You will need to compile Godot with: `scons platform=server tools=yes` (plus any
other arguements you'd normally use, like `-j` for thread counts and `bits=64`).
If you need "Godot Server", you will need `scons platform=server tools=yes`
instead, and is intended for things like Dedicated Game Servers.

Once you have Godot Headless and the build template for every target platform,
you'll need to modify your `.gitlab-ci.yml` file to be something like this:

```yml
image: registry.gitlab.com/greenfox/godot-build-automation:latest

build:
  script:
  - wget http://hostname.com/customGodotBuild.zip 
    #you can use GitLab to build Godot and use the pages url here^
  - unzip customGodotBuild.zip
  - cp ./.bin/godot_server.x11.opt.tools.64 /usr/local/bin/godot
    # this^ line replaces the godot version in this container with your custom build
  - builder
  artifacts:
    paths:
    - public/*

```

```yml
# where customGodotBuild.zip has:
.bin/godot_server.x11.opt.tools.64
.bin/godot.x11.opt.64
.bin/godot.windows.opt.64.exe
# You'll likely have things like Godot tools in this zip as well, that's fine
```

Then in the Godot editor, you'll need to bring up the Export menu, and point the
Custom Templates for each Export Preset to the appropriate `.bin/godot*` files.
Linux/X11 to `.bin/godot.x11.opt.64`, Windows to
`.bin/godot.windows.opt.64.exe`.

You could also use Git (or Git LFS) to just store these `.bin` files in your Git
Repo. That would be an easy & effective solution. However it is generally
"frowned upon" to store bins in a Git repo. If you don't often make changes to
your build (like if you're using someone else's modules), it's probably fine for
most projects. You'll still need the `cp ./.bin/godot_server.x11.opt.tools.64
/usr/local/bin/godot` line.
