Update: Godot 3.2.2 should work now.

# Build Automation for Godot in GitLab

This project started as tutorial for build automation. While working out how to
explain build automation for Godot, I realized most of the steps could be
completely automated for most Godot Projects. The project quickly evolved into a
complete build automation solution for Godot. As of writing this, I have used
this exact project (occasionally with minor tweaks) to build every Godot project
I have done.

If you are interested in learning how to build your own build automation system,
I believe `./build.sh` is *fairly* readable. Most of this script is geared
toward prepping Godot command line arguments and collecting artifacts.

This project is an experiment. I have created a docker container and a large
build script that should compile all Godot Headless compatible projects, however
I make no guarantees. If you find bugs in the system, **please** let me know
about your project and I will look into it.

[Link to example output!](https://greenfox.gitlab.io/godot-build-automation/)


# Prerequisites

## A Project That Is Compatible

If you are using GDNative, I'm pretty sure you will need write steps for
automating building your native modules and then this system might work.

OSX and Windows Universal build targets are untested. Mobile build targets are
not supported. Partly because I don't have the tools to test them and partly
because they are more complicated beyond the scope of this system. It is likely
that building for those targets is very possible with the tools I've created
here, but I personally do not have the time and tools to test and support it.

For most people, a pure GDScript Project targeting Linux, Windows, and HTML5
should work perfectly.

Godot with Mono (C#) is theoretically possible, but due to some technical
problems with it, at the moment I don't support it. See [C# in Known
Issues](#C\#).

## A Working Knowledge Of Git

I'm not going to use this project to teach Git. I'm going to assume your project
is already in a Git repo locally.

## GitLab

If you're using Git, you're probably using GitHub. However, GitHub didn't have a
build in Continuos Integration system at the time of creating this project. You
can use other CI systems with GitHub, but they require external set up. In the
interest of ease-of-use, I'm going to show how to build Godot from GitLab.

Adding your project to GitLab is very similar to adding it to GitHub. Create a
new repository or use an existing repository and add follow the instructions.

GitLab Free gives you 2,000 CI minutes per month so unless your project takes
more than a day every month to build, you're probably not going to run into
issues. If you do, consider building only on certain branches, buying Silver or
Gold tier, or starting your own GitLab Runner (this is what I do).

### Really want to keep using GitHub?

You can use GitHub as your Git repo host, but you'll still need GitLab for this
build automation system (although it's likely `./build.sh` can be adapted to
another tool like TravisCI). You'll still need a GitLab account, from the "new
project" page of GitLab, select "CI/CD for external repo" from the top of the
page to mirror your GitHub project.

## `.gitlab-ci.yml`

This should be everything you to add to build your project with GitLab. It will
pull a docker container that has Godot installed and the `./build.sh` script
that will extract data from your project.

## No `./public` Directory

`./public` is off limits for this build system to work as intended. If you have
a folder called `./public` in the root of your project, it will cause the build
to fail. `./public` is absolutely needed for GitLab Pages to work.

If you don't care about GitLab pages, there is a way to redirect your build
target and it is documented in `./build.sh -h`. For simplicity, I won't go over
that example unless it is requested.

## `export_presets.cfg`

By default, most projects will likely have `export_presets.cfg` in your
`.gitignore` file, leaving it out of your repository. This is recommended by
[godotengine/godot-demo-projects/.gitignore](https://github.com/godotengine/godot-demo-projects/blob/master/.gitignore).
I believe the reasoning behind this has to do with private keys used for some
build targets (like mobile).

For this build automation system to work, you need to include an
`export_presets.cfg` file in your projects. There may be a way to generate a bog
standard `export_presets.cfg` (with most build targets configured) if the
builder script does not find one. I have not implemented that yet, so for the
forseeable future, `export_presets.cfg` is still required.

## Working Build Targets

Currently, I have the tools at my disposal to test the following build targets:
- Linux/X11
- Windows Desktop
- HTML5

I have included, but have not tested, these build targets:
- Mac OSX
- Windows Universal

Other platforms (such as Android and iOS) are outside of my ability to test. I
am willing to try to expand this build system to those targets if is possible to
do so, but I will need help to that.

# How To Make This Work

- Add your project to GitLab.com
- Add the `.gitlab-ci.yml` in this repo to your repo.
- Have coffee

These steps are not numbered because they can be done in any order. 

**Yes, it really is that simple!**

Once you've committed your project to GitLab and added this `.gitlab-ci.yml`
file, it should just work. There is a banner on your GitLab project page listing
the last commit. a small circle well tell you the status of the build. Click
that circle to see the build pipeline. Yellow for Pending (gotta wait your turn
unless you pay for GitLab Bronze or better), Blue for Actively Building, Red for
failure [(see bottom)](#help-it-broke), and Green for success.

Once it's done, your project will be found on the web at
`https://${Your_GitLab_Name}.gitlab.io/${Your_Project_Name}` (from your project
page, click settings, pages, and there should be a direct link). This can be
disabled if you chose, but isn't by default. The first time your project builds,
this will not come up right away. It may take 15-60 minutes to create your
project page for the first time. However, after it has been created, it will
update almost as soon as your build completes.

# How does it work?

Every time you run a commit, GitLab will look into your project, read the
`.gitlab-ci.yml` run a build.

It starts with a docker image,
`registry.gitlab.com/greenfox/godot-build-automation`. This image is an Ubuntu
image with some extra packages installed as well as Godot. It extracts Godot
Headless to `/usr/local/bin/godot` and build templates to
`~/.local/share/godot/templates/`. If your not familar with docker, it's kind of
somewhere between a Virtual Machine and a Bare Metal application. It's has
virtualized networking and disk space, but it's still sharing the same kernel as
your base OS. If that doesn't make sense, don't worry, GitLab will handle it.

If you wish to design your own build automation system with the Headless version
of Godot, it's very obvious how to download Godot and run the binary form
wherever you've placed it. However, it is not obvious where the build templates
go. For `3.1.1`, the build templates are expected to be in
`~/.local/share/godot/templates/3.1.1.stable/` (for example, the path of
`windows_32_debug.exe` should be
`~/.local/share/godot/templates/3.1.1.stable/windows_32_debug.exe`)

Once the docker image is pulled, `builder` is called. This command is the script
in this repo called `./build.sh`. As a GitLab build, it will search for your
`project.godot` file in the current directory for your project's properties and
search `export_presets.cfg` to find your build targets. It will used the
information gathered there to build every export target you have defined.

Assuming Godot in installed correctly, the project is configured, and
${TARGET_DIR} exists, this one line does all the real work:<br> `godot -v
"${PROJECT_FILE}" --export "${EXPORT_NAME}"
"${TARGET_DIR}/${PROJECT_NAME}${FILE_ENDING}"`


Lastly, the script will zip up everything. This zipping step will be skipped for
any build target called `HTML5`, so that they can be played in a web browser
from GitLab.

# What Else Can I Do?

Right now this system will only build projects and will only build every build
target you have configured within Godot. You can use this build script to modify
how it builds, the project name, and a few other parameters. To do that, I
expect you have a working knowledge of Linux and Bash and can download
`./build.sh` and run it locally.

If you're willing to learn a bit about the `.gitlab-ci.yml` format, you can
trigger builds conditionally. For example only build when you merge back to
"master". Or only build when you have "BUILD ME" or some other text string in
your commit message. You can also configure a "conditional" build that requires
you to manually press a button; this is useful for choosing what builds to
publish to distribution.

# Help, It Broke

COOL!, I absolutely want to hear about every edge case that could break my
script. I've found a few projects, but certainly not everything. If your use
case is legitimate and you're willing to share with me a repo that can trigger
this issue then I will try to take the time to fix that.

# Known Issues

## C#
Until recantly, Godot did not publish a "headless" version of Godot with C#.
However, as of Godot3.2, they have. In theory, that means I should be able to
support Godot with Mono (C#) projects in this build automation system. However,
I'm seeing a lot of "it works on my machine" problems with the Mono-capible
docker containers. Godot projects that build just fine on my own Docker runners
are failing in GitLab. If anyone wants to help me debug this issue, I'd welcome
the help. However, I don't personally use C#, so it is difficult for me to
justify putting time into it.

# Advanced Topics

See [Advanced Topics](advanced_topics.md)

# `build.sh`/`builder` Release Notes

- 1.3
    - added -v to give you the version number
    - added Godot 3.2, see the new advanced section to change what your version
      is
- 1.2
    - Migrating docker container from Docker Hub to Gitlab
    - Adding scripts to compile Godot Engine itself (see
      [GodotCompiler/README.md](./GodotCompiler/README.md))
